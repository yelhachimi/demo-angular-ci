import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StatusComponent } from './status.component';
import { FormsModule } from '@angular/forms';
import { HeroService } from '../shared/services/hero.service';
import { StatusBarComponent } from './status-bar/status-bar.component';

describe('StatusComponent', () => {
  let component: StatusComponent;
  let fixture: ComponentFixture<StatusComponent>;
  let service: HeroService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatusComponent, StatusBarComponent ],
      imports: [FormsModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = TestBed.inject(HeroService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should add a hero', () => {
    
    let michel = { name: 'Michel', hp: 0, hpMax: 0 };
    component.newHero = michel;
    
    component.ajouterHero();
    expect(service.heros).toContain(michel);
  });

  it("should clear form after submit", ()=>{
    let michel = { name: 'Michel', hp: 0, hpMax: 0 };
    component.newHero = michel;
    component.ajouterHero();
    expect(component.newHero).toEqual({name:"", hp:0, hpMax:0});
  })
});
